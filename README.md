# scanutil
Util for scan your filesystem using rules

## rule
example:
```toml
[rule]
pkg="org.duangsuse.ls"
description="hello"
author=["duangsuse"]
rev=0
ver="0.1.0"
[[ls.rules]]
c="r" #replace
l="/tmp/a"
[[ls.rules]]
c="d" #delete
l="/tmp/b"
[[ls.rules]]
c="r"
l="/tmp/c"
[[ls.rules]]
c="d"
l="/tmp/d"
```

## usage
__scanutil [rules_path]__ //package list must be stored in rules_path with name '_pkgs_'
This will generate a _clean.lines_ file in rules_path. Edit it, and run scanutil again to clean.

## special
+ rules filenames must be {pkg}.toml

```toml
l="/tmp/b" #must be a file, not a dir, or scanutil may panic at run-time
```
__won't fix. this project is just a cross-compile&toml practice, next version of Ls will use C library to scan files__

## how to compile
on __ArchLinux__ `with` __rust toolchain__ & *rust std* for __armv7-linux-androideabi, i686-linux-android__ target & Android NDK installed at /opt/android-ndk
```bash
#prepare cargo cross-linker config
echo [target.armv7-linux-androideabi] >> ~/.cargo/config
echo linker="/opt/android-ndk/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-gcc" >> ~/.cargo/config

echo [target.i686-linux-android] >> ~/.cargo/config
echo linker="/opt/android-ndk/toolchains/x86-4.9/prebuilt/linux-x86_64/bin/i686-linux-android-gcc" >> ~/.cargo/config

#copy android platform libs to ~
cp -r /opt/android-ndk/platforms/android-16 ~

#chdir to arm libs
cd ~/android-16/arch-arm/usr/lib

#copy platform libs to compiler library path
cp libc.a libc.so liblog.so libdl.so libm.so libm.a /opt/android-ndk/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/arm-linux-androideabi/lib/armv7-a/
#same to x86

#start compiling in  android-{api}/arch-{arch}/usr/lib
cargo build --target armv7-linux-androideabi --manifest-path ~/ls_project/v0/scanutil/Cargo.toml 

```

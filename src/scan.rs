use std::io::Write;
use std::fs::{create_dir, remove_file, File};

use rules::Rules;

pub const CLEAN_FILENAME: &'static str = "clean.lines";

pub fn clean(config: Vec<CleanConfig>) -> Result<(), ()> {
    for f in config {
        let path: &str = &format!("{}", parse_path(f.path));
        println!("op path: {}", path);
        if f.replace {
            if let Ok(_) = remove_file(path) {
                println!("replacing {}", path);
                create_dir(path).unwrap();
            }
        } else {
            if let Ok(_) = remove_file(path) {
                println!("deleting {}", path);
            }
        }
    }
    Ok(())
}
pub fn scan(rules: Vec<Rules>) {
    let mut filterd = Vec::<String>::new();
    for r in rules {
        println!("scanning package {}", r.name);
        for s in r.scan {
            println!("scanning {}", s.path);
            if let Ok(f) = File::open(s.path) {
                println!("file {:?} exists", f);
                if s.default {
                    filterd.push(format!("r {:?}", f));
                } else {
                    filterd.push(format!("d {:?}", f));
                }
            }
        }
    }
    remove_file(CLEAN_FILENAME).unwrap_or_else(|e| println!("{}", e));
    if let Ok(mut f) = File::create(CLEAN_FILENAME) {
        let mut txt = String::new();
        for i in filterd {
            txt = format!("{}\n{}", txt, i);
        }
        println!("clean: {}", txt);
        f.write_all(txt.as_bytes()).unwrap();
    }
}

fn parse_path(dbg_print: String) -> String {
    let mut ret = String::new();
    let mut dquote_started = false;
    for c in dbg_print.chars() {
        if c == '"' {
            dquote_started = !dquote_started;
        }
        else if dquote_started {
            ret.push(c);
        }
    }
    ret
}
pub struct CleanConfig {
    pub replace: bool,
    pub path: String,
}
impl CleanConfig {
    pub fn new(replace: bool, path: String) -> CleanConfig {
        CleanConfig {
            replace: replace,
            path: path,
        }
    }
}

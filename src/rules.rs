use std::fs::{remove_file, File};
use std::io::prelude::*;
use std::process::exit;

use toml::from_str;

const PACKAGELIST_FIELNAME: &'static str = "pkgs";

pub fn scan_rules() -> Vec<Rules> {
    if let Ok(mut f) = File::open(PACKAGELIST_FIELNAME) {
        let mut rules = Vec::<Rules>::new();
        let mut buf: String = String::new();
        f.read_to_string(&mut buf).unwrap();
        for p in buf.lines() {
            if let Ok(mut f) = File::open(format!("{}{}", p, ".toml")) {
                println!("parsing rule {}", p);
                let mut buf = String::new();
                f.read_to_string(&mut buf).unwrap();
                rules.push(OrigPackage::from_str(&buf).to_easy());
            }
        }
        println!("{}", "deleting pkgfile...");
        remove_file(PACKAGELIST_FIELNAME).unwrap();
        rules
    } else {
        println!("{}", "cannot open package list");
        exit(3);
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct OrigPackage {
    pub rule: RuleInfo,
    pub ls: OrigRules,
}
impl OrigPackage {
    fn from_str(s: &str) -> Self {
        from_str(s).unwrap()
    }
    fn to_easy(self) -> Rules {
        let mut scans: Vec<ScanItem> = Vec::<ScanItem>::new();
        for i in self.ls.rules {
            scans.push(ScanItem::new(i.l, i.c == 'r'));
        }
        Rules::new(self.rule.pkg, scans)
    }
}
#[derive(Serialize, Deserialize, Debug)]
struct RuleInfo {
    pub pkg: String,
    pub description: String,
    pub author: Vec<String>,
    pub ver: String,
    pub rev: u16,
}
#[derive(Serialize, Deserialize, Debug)]
struct OrigRules {
    pub rules: Vec<Rule>,
}
#[derive(Serialize, Deserialize, Debug)]
struct Rule {
    pub c: char,
    pub l: String,
}

pub struct Rules {
    pub name: String,
    pub scan: Vec<ScanItem>,
}
impl Rules {
    fn new(name: String, scan: Vec<ScanItem>) -> Rules {
        Rules {
            name: name,
            scan: scan,
        }
    }
}

pub struct ScanItem {
    pub default: bool,
    pub path: String,
}
impl ScanItem {
    fn new(path: String, default: bool) -> ScanItem {
        ScanItem {
            default: default,
            path: path,
        }
    }
}

#[macro_use]
extern crate serde_derive;
extern crate toml;

mod rules;
mod scan;

use std::env::{args, current_dir, set_current_dir};
use std::process::exit;
use std::fs::remove_file;
use std::fs::File;
use std::io::prelude::*;
use scan::{CLEAN_FILENAME, scan, clean, CleanConfig};
use rules::scan_rules;

const VERSION: &'static str = "0.1.0";

fn main() {
    println!("ls scanutil ver {} (just a test, C rewrite wanted)", VERSION);
    let home_dir_loc = args().skip(1).next().unwrap_or_else(|| exit(1));
    if let Ok(_) = set_current_dir(home_dir_loc) {
        println!("config dir: {:?}", current_dir().unwrap().as_path());
        if let Ok(mut f) = File::open(CLEAN_FILENAME) {
            let mut content = String::new();
            f.read_to_string(&mut content).unwrap();
            println!("cleaning {} items...", content.lines().count());
            let mut clean_config = Vec::<CleanConfig>::new();
            for l in content.lines() {
                print!("package {}: ", l);
                if l.starts_with("r ") {
                    println!("{}", "replace");
                    let mut line = l.to_string();
                    strip_cfg(&mut line);
                    clean_config.push(CleanConfig::new(true, line));
                } else if l.starts_with("d ") {
                    println!("{}", "delete");
                    let mut line = l.to_string();
                    strip_cfg(&mut line);
                    clean_config.push(CleanConfig::new(false, line));
                } else {
                    println!("{} ignored", l);
                }
            }
            clean(clean_config).unwrap();
            remove_file(CLEAN_FILENAME).unwrap();
        } else {
            println!(
                "parsing {} rules...",
                current_dir().unwrap().read_dir().unwrap().count()
            );
            let my_rules = scan_rules();
            scan(my_rules);
        }
    } else {
        println!("Dir does not exists.");
        exit(2);
    }
}

fn strip_cfg(o: &mut String) {
    o.remove(0);
    o.remove(0);
    o.shrink_to_fit();
}
